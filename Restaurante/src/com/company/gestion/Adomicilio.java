package com.company.gestion;
import com.company.productos.Producto;
import com.company.usuarios.Cliente;

interface Envio {
    //Pensar si implementar tambien a Local
     double envio = 0.1;
     double propina = 0.1;

     double calcularPrecioFinal(double subtotal);

}


public class Adomicilio extends Orden implements Envio{


    protected Cliente cliente;

    //CONSTRUCTORES
    public Adomicilio(Producto[] listaProductos, Cliente cliente) {
        super(listaProductos);
        this.cliente = cliente;
    }

    //GETTERS

    //SETTER


    @Override
    public String toString() {
        return "A DOMICILIO: \n" +
                cliente.toString();
    }

    @Override
    public double calcularPrecioFinal(double subtotal) {
        return (subtotal * envio) + (subtotal * propina) + subtotal;
    }
}
