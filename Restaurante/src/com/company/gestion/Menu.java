package com.company.gestion;
import com.company.productos.Producto;
import com.company.teclado.Teclado;

public class Menu {

    //Constructor vacio
    public Menu() {}

    private final int num = 40; //Hasta 40 platillos en el menu
    private int numProducto = 0;
    private final Producto[] listaProductos = new Producto[num];
    private final Integer[] listaID = new Integer[num];


    //Metodos

    public void agregarProducto(){
        Producto producto;
        String nombre;
        String descripcion;
        String categoria;
        double precio;
        int id = 0;
        int contador = 0;
        boolean valido = false;
        System.out.println("Ingrese nombre del producto: ");
        nombre = Teclado.LeeCadena();
        System.out.println("Ingrese la descripcion del producto: ");
        descripcion = Teclado.LeeCadena();
        //Seleccionar la categoria
        categoria = seleccionarCategoria();
        System.out.println("Ingrese el precio: ");
        precio = Teclado.LeeDouble();
        //Para solicitar y validar id
        while(!valido){
            if (contador == 0) {
                System.out.println("Ingresa un ID de 3 dígitos(entre 100 y 999)");
            }
            id = Teclado.LeeEntero();
            while((id<100)||(id>999)){
                System.out.println("Numero fuera de rango. El rango es de 100 a 999");
                System.out.println("Ingrese otro número: ");
                id = Teclado.LeeEntero();
            }
            valido = validarID(id);
            contador++;
        }
        producto = new Producto(nombre, descripcion, categoria, precio, id);
        //Agrega producto al array
        insertarProducto(producto);
    }

    //Funcion auxiliar de agregarProductos
    private boolean validarID(Integer id){
        boolean valido = true;

        for(int i = 0; i< listaID.length; i++){
            if (id.equals(listaID[i])){
                System.out.println("ID repetido. Ingrese otro id");
                valido = false;
            }
        }
        return valido;
    }

    //Funcion auxiliar de ver productos
    private String seleccionarCategoria(){

        int opcion;
        String categoria = null;
        System.out.println("-------------------------------------------");
        System.out.println("Seleccione una de las siguientes categorias");
        System.out.println(" 1. Entradas");
        System.out.println(" 2. Ensaladas");
        System.out.println(" 3. Sopas");
        System.out.println(" 4. Platillos");
        System.out.println(" 5. Postres");
        System.out.println(" 6. Bebidas");

        //
        do {
            opcion = Teclado.LeeEntero();
            if((opcion < 1 )|| (opcion >6)){
                System.out.println("La opcion ingresada no es válida");
            }
        }while((opcion < 1 )|| (opcion >6));

        //Asinar la categoria
        if(opcion == 1){
            categoria = "Entradas";
        }
        if(opcion == 2){
            categoria = "Ensaladas";
        }
        if(opcion == 3){
            categoria = "Sopas";
        }
        if(opcion == 4){
            categoria = "Platillos";
        }
        if(opcion == 5){
            categoria = "Postres";
        }
        if(opcion == 6){
            categoria = "Bebidas";
        }
        return categoria;

    }

    //Se puede usar para agregar productos desde el menu o sin el menu
    public void insertarProducto(Producto producto){
        listaProductos[numProducto] = producto;
        listaID[numProducto] = producto.getID();
        numProducto ++;
    }

    public void verProductos(){
        String categoria;
        boolean encontrado = false ;

            categoria = seleccionarCategoria();
            for (Producto p : listaProductos) {
                if (p == null) {
                    break;
                } else {
                    if (categoria.equals(p.getCategoria())) {
                        //USAR TOSTRING MAS ADELANTE
                        System.out.println("--------------------------------------------");
                        System.out.println("Producto: " + p.getNombre());
                        System.out.println("Descripcion: " + p.getDescripcion());
                        System.out.println("Precio: " + p.getPrecio());
                        System.out.println("ID: " + p.getID());
                        encontrado = true;
                    }

                }

            }

        System.out.println("--------------------------------------------");
        if(!encontrado){
            System.out.println("Aun no hay productos en la categoria seleccionada.");
        }
        }


    public Producto seleccionarProducto(){
        Producto prod_temporal = null;
        Producto prod_elegido;
        Integer id = 0;
        int contador = 0;
        int cantidad;
        double precioparcial;
        boolean encontrado= false;

        //Validar que el id exista
        while(!encontrado){

            if (contador == 0) {
                verProductos();
                System.out.println("Ingresa el ID del producto que deseas agregar a la orden");
            }
            id = Teclado.LeeEntero();
            while((id<100)||(id>999)){
                System.out.println("Numero fuera de rango. El rango es de 100 a 999");
                System.out.println("Ingrese otro número: ");
                id = Teclado.LeeEntero();
            }
            encontrado = encontrado(id);
            contador++;
        }

        //Imprimir producto seleccionado
        for (Producto p : listaProductos) {
            if (p == null) {
                break;
            } else {

                if (id.equals(p.getID())) {
                    prod_temporal = p;
                    System.out.println("--------------------------------------------");
                    System.out.println("Producto: " + p.getNombre());
                    System.out.println("Descripcion: " + p.getDescripcion());
                    System.out.println("Precio: " + p.getPrecio());
                }
            }
        }

        System.out.println("Ingrese la cantidad: ");
        do {
            cantidad = Teclado.LeeEntero();
            if(cantidad <= 0){
                System.out.println("Ingrese cantidad mayor a 0");
            }
            if(cantidad >= 11){
                System.out.println("Ingrese máximo 10 unidades");
            }
        }while ((cantidad <=0) || (cantidad >10));

        //******//
        assert prod_temporal != null;
        precioparcial=prod_temporal.getPrecio()* cantidad;

        //Llenamos objeto con constructor de cantidad
        prod_elegido = new Producto(prod_temporal.getNombre(), prod_temporal.getDescripcion(), cantidad, prod_temporal.getPrecio());

        prod_elegido.setPrecio_seleccion(precioparcial);
        //Validar que el se agregue el
        int opt;
        System.out.println("¿Desea agregar el producto?");
        System.out.println(prod_elegido.toString());
        System.out.println("[1] SI");
        System.out.println("[2] NO");
        opt = Teclado.LeeEntero();

        if(opt == 2) {prod_elegido = null;}

        return prod_elegido;
    }

    public boolean encontrado(Integer id){
        boolean encontrado = false;

        for(int i = 0; i< listaID.length; i++){
            if (id.equals(listaID[i])){
                encontrado = true;
            }
        }
        if(!encontrado)
        {
            System.out.println("No se encontró el id. Ingrese uno válido");
        }
        return encontrado;
    }
    }


