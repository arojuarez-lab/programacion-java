package com.company.gestion;
import com.company.productos.*;

public class Orden {
    private final int num = 20; //Hasta 20 productos por orden
    private int numProducto;
    private Producto[] listaProductos = new Producto[num];
    int id;
    double total;
    double subtotal;

    //Constructor

    public Orden(Producto[] listaProductos) {
        this.listaProductos = listaProductos;
        this.numProducto = 0;
    }

    public Orden(){

    }

    //METODOS
    public void insertarProducto(Producto producto){
        listaProductos[numProducto] = producto;
        numProducto ++;
    }

    public void mostrarOrden(){
        System.out.println("----------------------------------");
        System.out.print("Resumen de la orden: ");
        for ( Producto p : listaProductos) {
            if (p == null) {
                break;
            }
            else{
                System.out.println();
                System.out.println(p.toString());
            }
        }
    }

    //Getters
    public Producto[] getListaProductos() {
        return listaProductos;
    }

    public int getId() {
        return id;
    }

    public final double getSubtotal() {
        return subtotal;
    }

    //SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double calculaTotal(){
        total = (subtotal * .1) + subtotal;
        return total;
    }

}
