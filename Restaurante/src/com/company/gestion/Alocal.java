package com.company.gestion;

import com.company.productos.Producto;

public class Alocal extends Orden{

    private int numeroMesa;
    private String mesero;


    //Constructor
    public Alocal(){}
    public Alocal(Producto[] listaProductos, int numeroMesa, String mesero) {
        super(listaProductos);
        this.numeroMesa = numeroMesa;
        this.mesero = mesero;
    }


    //Getters
    public int getNumeroMesa() {
        return numeroMesa;
    }

    public String getMesero() {
        return mesero;
    }

    //Setters
    public void setNumeroMesa(int numeroMesa) {
        this.numeroMesa = numeroMesa;
    }

    public void setMesero(String mesero) {
        this.mesero = mesero;
    }


    //Otros metodos

    @Override
    public String toString() {
        return "A local: \n" +
                "numeroMesa: " + numeroMesa +
                ", mesero: '" + mesero + '\'' +
                ']';
    }
}

