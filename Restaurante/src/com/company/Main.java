package com.company;
import com.company.administracion.*;
import com.company.gestion.*;
import com.company.teclado.Teclado;
import com.company.usuarios.*;
import java.util.ArrayList;

import static com.company.administracion.Restaurante.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {
    static Menu menu = Restaurante.insertarProductos();
    static ArrayList<Orden> ordenes = new ArrayList<>();
    static ArrayList<Integer> ordenesId = new ArrayList<>();
    static ArrayList<Cliente> clientes = new ArrayList<>();



    public static void main(String[] args) {
        //Declaracion de variables
        String nombre;
        String apellido;
        Empleado empleado;
        //Solicitar nombre del trabajador
        System.out.println("Ingresa tu nombre: ");
        nombre = Teclado.LeeCadena();
        System.out.println("Ingresa tu apellido: ");
        apellido = Teclado.LeeCadena();
        empleado = new Empleado(nombre, apellido);



        menuPrincipal(empleado);

    }


    public static void menuPrincipal(Empleado empleado){
        LocalDateTime localDate = LocalDateTime.now();
        System.out.println("********************************************");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM, yyyy \nh:mm a");
        System.out.println(dtf.format(localDate));
        System.out.println("Bienvenido: " + empleado.getNombre());
        int opt;

        do{
            System.out.println("********************************************");
            System.out.println("\t.:MENU PRINCIPAL:.");
            System.out.println("1. Carta/menu del restaurante");
            System.out.println("2. Ordenes");
            System.out.println("3. Ver ingresos");
            System.out.println("4. SALIR");
            System.out.println("********************************************");
            System.out.print("Opcion: ");
            opt = Teclado.LeeEntero();


            switch(opt) {
                case 1:
                    menuCarta();
                    break;
                case 2:
                    menuOrdenes();
                    break;
                case 3:
                    verIngresos(ordenes);
                    break;
                case 4:
                    break;

                default:
                    System.out.println("Opcion equivocada.");
                    break;

            }
        }while(opt!=4);
    }

    public static void menuCarta(){
        int opt;

        do{
            System.out.println("********************************************");
            System.out.println("\t.:MENU DE PLATILLOS:.");
            System.out.println("1. Ver menu de platillos");
            System.out.println("2. Agregar platillo");
            System.out.println("3. SALIR");
            System.out.println("********************************************");
            System.out.print("Opcion: ");
            opt = Teclado.LeeEntero();

            switch(opt){
                case 1:
                    menu.verProductos();
                    break;
                case 2:
                    menu.agregarProducto();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Opcion equivocada.");
                    break;

            }
        }while(opt!=3);
        }

    public static void menuOrdenes(){
        int opt;

        do{
            System.out.println("********************************************");
            System.out.println("\t.:GESTION DE ORDENES:.");
            System.out.println("1. Crear nueva orden");
            System.out.println("2. Ver ordenes a domicilio");
            System.out.println("3. Ver ordenes a local");
            System.out.println("4. Ver listado de ordenes");
            System.out.println("5. Buscar orden por id");
            System.out.println("6. SALIR");
            System.out.println("********************************************");
            System.out.print("Opcion: ");
            opt = Teclado.LeeEntero();


            switch(opt) {
                case 1:
                    ordenesOpcion1();
                    break;
                case 2:
                    resumenODomicilio(ordenes);
                    break;
                case 3:
                    resumenOLocal(ordenes);
                    break;
                case 4:
                    listarOrdenes(ordenes);
                    break;
                case 5:
                    buscarOrden(ordenes, ordenesId);
                    break;
                case 6:
                break;

                default:
                    System.out.println("Opcion equivocada.");
                    break;

            }
        }while(opt!=6);


        //ordenes.add(Restaurante.agregarProductosOrden(menu));


    }


    static void ordenesOpcion1(){
        int opt_secundaria;
        do {
            System.out.println("[1] Orden local");
            System.out.println("[2] Orden a domicilio");
            System.out.println("[3] SALIR");
            opt_secundaria = Teclado.LeeEntero();
            while ((opt_secundaria < 1) || (opt_secundaria > 3)) {
                System.out.println("Ingresaste una opción inválida. Vuelve a intentar: ");
                opt_secundaria = Teclado.LeeEntero();
            }
            if (opt_secundaria == 1) {
                ordenes.add(Restaurante.crearOrdenLocal(Restaurante.agregarProductosOrden(menu), ordenesId));
            }
            if(opt_secundaria == 2) {
                //Primero agregamos cliente
                Cliente cliente_temporal;
                cliente_temporal = agregarCliente(clientes);
                ordenes.add(Restaurante.crearOrdenDomicilio(Restaurante.agregarProductosOrden(menu), cliente_temporal, ordenesId));
            }
            System.out.println("Deseas crear otra orden");
            System.out.println("[1] SI");
            System.out.println("[2] NO");
            opt_secundaria = Teclado.LeeEntero();
            while ((opt_secundaria < 1) || (opt_secundaria > 2)) {
                System.out.println("Ingresaste una opción inválida. Vuelve a intentar: ");
                opt_secundaria = Teclado.LeeEntero();
            }
            if(opt_secundaria == 2){
                opt_secundaria = 3;
            }

        }while(opt_secundaria!=3);
    }


}

