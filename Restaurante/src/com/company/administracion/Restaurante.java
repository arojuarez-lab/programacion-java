package com.company.administracion;
import com.company.gestion.*;
import com.company.productos.Producto;
import com.company.teclado.Teclado;
import com.company.usuarios.Cliente;
import java.util.ArrayList;


public abstract class Restaurante {

    //Agregamos los platillos al menu
    public static Menu insertarProductos() {
        Menu menu = new Menu();
        //Entradas
        menu.insertarProducto(new Producto("Alitas clásicas", "Orden de 6 alitas con salsa búfalo", "Entradas", 100.0, 100));
        menu.insertarProducto(new Producto("Fondue de espinacas", "Fondue de queso manchego", "Entradas", 80.0, 102));
        menu.insertarProducto(new Producto("Brocheta de queso (2 pz)", "Brocheta de queso manchego empanizada", "Entradas", 60, 104));

        //Ensaladas
        menu.insertarProducto(new Producto("Ensalada campestre", "Ensalada con tocino y hongos", "Ensaladas", 120.0, 200));
        menu.insertarProducto(new Producto("Ensalada de la casa", "Mezcla de lechugas con frutos rojos", "Ensaladas", 130.0, 202));
        menu.insertarProducto(new Producto("Ensalada césar", "Mezcla de lechugas y quesos", "Ensaladas", 140, 204));

        //Sopas
        menu.insertarProducto(new Producto("Sopa de la casa", " Sopa de tallarines y jitomate ", "Sopas", 60.0, 300));
        menu.insertarProducto(new Producto("Sopa de queso", " Sopa de queso panela en caldo de jitomate", "Sopas", 70.0, 302));
        menu.insertarProducto(new Producto("Consomé de la casa", "Consomé co arroz", "Sopas", 65, 304));

        //Platillos
        menu.insertarProducto(new Producto("Hamburguesa con papas", "Hamburguesa con papas fritas ", "Platillos", 140.0, 400));
        menu.insertarProducto(new Producto("Orden de boneless", "Trozos de pechuga empanizada", "Platillos", 140.0, 402));
        menu.insertarProducto(new Producto("Quesadillas", "Quesadillas con pico de gallo", "Platillos", 100, 404));

        //Postres
        menu.insertarProducto(new Producto("Helado del bosque", "Helado de frutos rojos con nuez", "Postres", 60.0, 500));
        menu.insertarProducto(new Producto("Pastel de la sierra", "Pastel de elote bañado con mezcal", "Postres", 80.0, 502));
        menu.insertarProducto(new Producto("Flan de la casa", "Flan bañado con leche dulce", "Postres", 40, 504));

        //Bebidas
        menu.insertarProducto(new Producto("Limonada", "Limonada mineral", "Bebidas", 40.0, 600));
        menu.insertarProducto(new Producto("Refresco", "Refresco de lata(sabores variados)", "Bebidas", 35, 602));
        menu.insertarProducto(new Producto("Agua del dia", "Agua de naranja", "Bebidas", 40, 604));


        return menu;
    }


    public static Orden agregarProductosOrden(Menu menu) {
        int opt;
        double subtotal = 0;
        Orden orden = new Orden();
        do {
            Producto p = null;
            while (p == null) {
                p = menu.seleccionarProducto();
            }
            //Agregamos producto a la orden
            orden.insertarProducto(p);

            //
            subtotal = p.getPrecio_seleccion() + subtotal;

            System.out.println("¿Desee agregar otro producto a la orden?");
            System.out.println("[1] SI");
            System.out.println("[2] NO");
            opt = Teclado.LeeEntero();
            //Validación
            while ((opt > 2) || (opt < 1)) {
                System.out.println("Ingresa una opción valida");
                opt = Teclado.LeeEntero();
            }
        } while (opt == 1);

        System.out.println("Subtotal: " + subtotal);
        orden.setSubtotal(subtotal);
        return orden;
    }

    public static Cliente agregarCliente(ArrayList<Cliente> clientes) {
        String nombre;
        int id;
        String direccion;
        Cliente cliente;

        System.out.println("Ingrese el nombre del cliente");
        nombre = Teclado.LeeCadena();
        //generarId
        id = generarIdCliente(clientes);
        System.out.println("Ingrese la direccion del cliente");
        direccion = Teclado.LeeCadena();

        cliente = new Cliente(nombre, id, direccion);
        return cliente;

    }

    public static Adomicilio crearOrdenDomicilio(Orden orden, Cliente cliente, ArrayList<Integer> ordenesId) {
        int id;
        Adomicilio adomicilio;
        adomicilio = new Adomicilio(orden.getListaProductos(), cliente);

        adomicilio.setSubtotal(orden.getSubtotal());

        id = generarId(ordenesId);
        adomicilio.setId(id);
        ordenesId.add(id);

        return adomicilio;
    }

    public static Alocal crearOrdenLocal(Orden orden, ArrayList<Integer> ordenId) {
        int mesa;
        int id;
        Alocal local;
        String mesero;
        System.out.println("Ingresa el numero de mesa (1-20)");
        mesa = Teclado.LeeEntero();
        while ((mesa > 20) || (mesa < 1)) {
            System.out.println("Ingresa una opción valida (1-20)");
            mesa = Teclado.LeeEntero();
        }
        //Creamos y guardamos id en la lista de id
        id = generarId(ordenId);
        ordenId.add(id);

        System.out.println("Ingrese el nombre del mesero");
        mesero = Teclado.LeeCadena();

        local = new Alocal(orden.getListaProductos(), mesa, mesero);
        local.setSubtotal(orden.getSubtotal());
        //UPCASTING   ((Orden)local).setId(id);
        local.setId(id);
        return local;

    }

    public static int generarId(ArrayList<Integer> ordenId) {
        int id;
        boolean existe;
        do {
            System.out.println("Ingresa un id para la orden (1000-9999)");
            id = Teclado.LeeEntero();
            while ((id > 9999) || (id < 1000)) {
                System.out.println("Ingresa una opción valida (1000-9999)");
                id = Teclado.LeeEntero();
            }
            existe = validarId(id, ordenId);
        } while (existe);

        return id;
    }

    public static boolean validarId(int id, ArrayList<Integer> ordenId) {
        boolean existe = false;
        for (int i = 0; i < ordenId.size(); i++) {
            if (id == ordenId.get(i)) {
                existe = true;
            }
        }
        return existe;
    }


    public static int generarIdCliente(ArrayList<Cliente> clientes) {
        int id;
        boolean existe;
        do {
            System.out.println("Ingresa un id para el cliente (1000-9999)");
            id = Teclado.LeeEntero();
            while ((id > 9999) || (id < 1000)) {
                System.out.println("Ingresa una opción valida (1000-9999)");
                id = Teclado.LeeEntero();
            }
            existe = validarIdCliente(id, clientes);
        } while (existe);

        return id;
    }

    public static boolean validarIdCliente(int id, ArrayList<Cliente> clientes) {
        boolean existe = false;

        for (int i = 0; i < clientes.size(); i++) {
            if (clientes.get(i).getId() == id) {
                existe = true;
            }
        }
        return existe;
    }

    public static void resumenODomicilio(ArrayList<Orden> ordenes) {

        if (ordenes == null) {
            System.out.println("Aún no hay ordenes. Cree una");
        } else {
            for (Orden orden : ordenes) {
                if (orden instanceof Adomicilio) {
                    System.out.println("------------------------------------");
                    System.out.println("ORDEN ID: " + orden.getId());
                    //Downcasting
                    System.out.println((orden.toString()));
                    orden.mostrarOrden();
                    System.out.println("Subtotal: " + (orden.getSubtotal()));
                    System.out.println("Total: " + ((((Adomicilio) orden).calcularPrecioFinal((orden.getSubtotal())))));
                    System.out.println("------------------------------------");

                }
            }
        }
    }


    public static void resumenOLocal(ArrayList<Orden> ordenes) {

        if (ordenes == null) {
            System.out.println("Aún no hay ordenes. Cree una");
        } else {
            for (Orden orden : ordenes) {
                if (orden instanceof Alocal) {
                    System.out.println("------------------------------------");
                    System.out.println("ORDEN ID: " + orden.getId());
                    //Downcasting
                    System.out.println((((Alocal) orden).toString()));
                    orden.mostrarOrden();
                    System.out.println("Subtotal: " + orden.getSubtotal());
                    System.out.println("Total: " + orden.calculaTotal());
                    System.out.println("------------------------------------");

                }
            }
        }
    }

    public static void listarOrdenes(ArrayList<Orden> ordenes) {
        if (ordenes == null) {
            System.out.println("Aún no hay ordenes. Cree una");
        } else {
            for (Orden orden : ordenes) {
                if (orden instanceof Alocal) {
                    System.out.println("------------------------------------");
                    System.out.println("ORDEN ID: " + orden.getId());
                }
                if (orden instanceof Adomicilio) {
                    System.out.println("------------------------------------");
                    System.out.println("ORDEN ID: " + orden.getId());
                }
            }


        }

        //public static Adomicilio crearOrdenDomicilio(Orden orden){}
    }

    public static void buscarOrden(ArrayList<Orden> ordenes, ArrayList<Integer> ordenesId) {
        boolean existe;
        int id;

        do {
            System.out.println("Ingresa el Id de la orden que deseas desglosar");
            id = Teclado.LeeEntero();
            existe = validarId(id, ordenesId);
            if (!existe) {
                System.out.println("Ingresa un id valido");
            }
        } while (!existe);

        for (Orden orden : ordenes) {

            if (id == orden.getId()) {
                if (orden instanceof Alocal) {
                    System.out.println("------------------------------------");
                    System.out.println("ORDEN ID: " + orden.getId());
                    //Downcasting
                    System.out.println((((Alocal) orden).toString()));
                    orden.mostrarOrden();
                    System.out.println("Subtotal: " + orden.getSubtotal());
                    System.out.println("Local: " + orden.calculaTotal());
                    System.out.println("------------------------------------");
                }

                if (orden instanceof Adomicilio) {
                    System.out.println("------------------------------------");
                    System.out.println("ORDEN ID: " + orden.getId());
                    //Downcasting
                    System.out.println((orden.toString()));
                    orden.mostrarOrden();
                    System.out.println("Subtotal: " + (orden.getSubtotal()));
                    System.out.println("Total: " + ((((Adomicilio) orden).calcularPrecioFinal((orden.getSubtotal())))));
                    System.out.println("------------------------------------");

                }
            }


        }
    }


    public static void verIngresos(ArrayList<Orden> ordenes){
        double gananciasT;
        double gananciasN = 0;
        double propinas = 0;
        int noOrdenes = 0;

        for (Orden o: ordenes){
            gananciasN=gananciasN + o.getSubtotal();
            propinas=propinas + (o.getSubtotal() * .1);
            noOrdenes ++;
        }
        gananciasT= gananciasN + propinas;


        System.out.println("BUEN DIA");
        System.out.println("La ganancia neta es igual a: " + gananciasN + " mxn ");
        System.out.println("La ganancia total (con propinas) es igual a: " + gananciasT + " mxn ");
        System.out.println("Tu pago al día de hoy es de: " + (propinas/3) + " mxn ");
        System.out.println("No. de ordenes del día de hoy: " + noOrdenes);


    }
}