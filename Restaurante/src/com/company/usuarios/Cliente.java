package com.company.usuarios;

public class Cliente extends Persona{

    private final int id;
    private final String direccion;

    public Cliente(String nombre, int id, String direccion) {
        super(nombre);
        this.id = id;
        this.direccion = direccion;
    }

    @Override
    public String getNombre(){
        return nombre;
    }


    public int getId(){
        return id;
    }



    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", direccion='" + direccion + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
