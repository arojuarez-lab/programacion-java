package com.company.usuarios;

public abstract class Persona {

    //Atributos
    protected String nombre;

    public Persona(String nombre) {
        this.nombre = nombre;

    }

    public abstract String getNombre();


}
