package com.company.usuarios;

public class Empleado extends Persona{

    private String apellido;

    public Empleado(String nombre, String apellido) {
        super(nombre);
        this.apellido = apellido;
    }

    @Override
    public String getNombre(){
        return nombre + " " + apellido;
    }



}
