package com.company.productos;

public final class Producto {
    private int cantidad = 0;
    private final double precio;
    public double precio_seleccion;
    private final String nombre;
    private final String descripcion;
    private String categoria;
    private Integer id;

    //CONSTRUCTORES
    public Producto(String nombre, String descripcion, int cantidad, double precio) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public Producto(String nombre, String descripcion, String categoria, double precio, Integer id) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.precio = precio;
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCategoria() {
        return categoria;
    }


    public double getPrecio(){
        return precio;
    }


    public int getID(){
        return id;
    }

    public double getPrecio_seleccion(){
        return precio_seleccion;
    }

    public void setPrecio_seleccion(double precio_seleccion) {
        this.precio_seleccion = precio_seleccion;
    }

    @Override
    public String toString() {
        return  "Nombre: " + nombre + '\'' +
                ", Descripcion: '" + descripcion + '\'' +
                ", Precio: '" + precio + '\'' +
                ", Cantidad: " + cantidad + '\'';
    }


}
