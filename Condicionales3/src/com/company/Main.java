package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        //Contraseña correcta
        String contraseñaCorrecta = "qwerty";

	    //Solicitar la contraseña
        System.out.println("Hola Arturo, ingresa tu contraseña");
        Scanner entrada = new Scanner(System.in);
        String contraseñaUsuario = entrada.nextLine();

        //Ecaluar si la contraseña es "qwerty"
        if(contraseñaUsuario.toLowerCase().equals(contraseñaCorrecta.toLowerCase()))
        {
            System.out.println("Acceso concedido");
        }
        else
        {
            System.out.println("Contraseña invalida, acceso denegado.");
        }
        //Si sí, acceso concedido
        //Sí no, acceso denegado
    }
}
