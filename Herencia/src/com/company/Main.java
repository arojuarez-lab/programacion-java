package com.company;

public class Main
{
    public static void main(String[] args)
    {
        //Generar una persona
        Persona p = new Persona("Arturo", "Juárez", 29);
        //Imprimir datos Persona
        p.mostrarDatos();

        //Generar un estudiante
        Estudiante e = new Estudiante("Arturo", "Juárez", 29, 10, 8);
        //Imprimir datos estudiante
        e.mostrarDatos();

    }
}