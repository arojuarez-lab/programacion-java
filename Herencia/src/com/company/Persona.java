package com.company;

public class Persona
{
    private String nombre;
    private String apellido;
    private int edad;

    public Persona(String nombre, String apellido, int edad)
    {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    //Métodos Getters
    String getNombre()
    {
        return nombre;
    }

    String getApellido()
    {
        return apellido;
    }

    int getEdad()
    {
        return edad;
    }

    //Métodos Setters
    void setNombre(String x)
    {
        nombre = x;
    }

    void setApellido(String x)
    {
        apellido = x;
    }

    void setEdad(int x)
    {
        edad = x;
    }

    void mostrarDatos()
    {
        System.out.println("Nombre: " + getNombre() + "Apellido: " + getApellido() + "Edad: " + getEdad());
    }
}