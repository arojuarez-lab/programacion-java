package com.company;

public class Estudiante extends Persona
{
    private int codigoEstudiante;
    private float notaFinal;

    public Estudiante(String nombre, String apellido, int edad, int codigoEstudiante, float notaFinal)
    {
        super(nombre, apellido, edad);
        this.codigoEstudiante = codigoEstudiante;
        this.notaFinal = notaFinal;
    }

    //Métodos Getters
    String getNombre()
    {
        return nombre;
    }

    String getApellido()
    {
        return apellido;
    }

    int edad()
    {
        return edad;
    }

    //Métodos Setters
    void setNombre(String x)
    {
        nombre = x;
    }

    void setApellido(String x)
    {
        apellido = x;
    }

    void setEdad(int x)
    {
        edad = x;
    }

    //Sobreescritura de mostrar datos

    @Override
    void mostrarDatos()
    {
        super.mostrarDatos();
    }
}
