package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Teclado
{
    //Caracter
    public static char LeerCaracter()
    {
        char ch;

        try
        {
            ch = LeerCadena().charAt(0);
        }
        catch (Exception e)
        {
            ch = '\0';
        }
        return ch;
    }

    //String
    public static String LeerCadena()
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        try
        {
            str = br.readLine();
        }
        catch (Exception e)
        {
            str = "";
        }
        return str;
    }

    //Entero
    public static int LeerEntero()
    {
        int num1;
        try
        {
            num1 = Integer.parseInt(LeerCadena().trim());
        }
        catch (Exception e)
        {
            num1 = 0;
        }
        return num1;
    }

    public static float LeerFloat()
    {
        float calificacion;
        try
        {
            calificacion = Float.parseFloat(LeerCadena().trim());
        }
        catch (Exception e)
        {
            calificacion = 0;
        }
        return calificacion;
    }

    public static double LeerDouble()
    {
        {
            double num2;
            try
            {
                num2 = Double.parseDouble(LeerCadena().trim());
            }
            catch (Exception e)
            {
                num2 = 0;
            }
            return num2;
        }

    }
}
