package com.company;

import java.util.ArrayList;

public class Almacen
{
    private ArrayList<Producto> almacen = new ArrayList<Producto>();

    boolean estaVacio()
    {
        return almacen.isEmpty();
    }

    void listarProducto(int opcion)
    {

        if (opcion == 1)
        {
            if(almacen.isEmpty())
            {
                System.out.println("Aun no hay productos en el inventario. Agrega uno");
            }
            else
            {
                for(Producto p : almacen)
                {
                    System.out.println(p.getSKU() + " - " + p.getNombre() + " - " + p.getCantidad());
                }
            }
        }

        if(opcion == 2)
        {
            if(almacen.isEmpty())
            {
                System.out.println("Aun no hay productos en el inventario. Agrega uno");
            }
            else
            {
                for(Producto p : almacen)
                {
                    if(p.getCantidad() >0)
                    {
                        System.out.println(p.getSKU() + " - " + p.getNombre() + " - " + p.getDescripcion());
                    }
                }
            }
        }

        if (opcion == 3)
        {
            if (almacen.isEmpty())
            {
                System.out.println("Aun no hay productos en el inventario. Agrega uno");
            }
            else
            {
                for (Producto p : almacen)
                {
                    if(p.getCantidad() ==0)
                    {
                        System.out.println(p.getSKU() + " - " + p.getNombre() + " - " + p.getCantidad());
                    }
                }
            }
        }
    }

    void generarProducto()
    {
        int sku, cantidad;
        String nombre, descripcion;
        double precio;

        //SKU
        System.out.println("Ingresa el SKU del producto");
        sku = Teclado.LeerEntero();

        //Nombre
        System.out.println("Ingresa el nombre del producto");
        nombre = Teclado.LeerCadena();

        //Descripción
        System.out.println("Ingresa su descripción");
        descripcion = Teclado.LeerCadena();

        //Cantidad
        System.out.println("Ingresa la cantidad #");
        cantidad = Teclado.LeerEntero();

        //Precio
        System.out.println("Ingresa el precio $");
        precio = Teclado.LeerDouble();

        Producto producto = new Producto(sku, nombre, descripcion, cantidad, precio);
        almacen.add(producto);
    }

    void agregarProducto(Producto p)
    {
        almacen.add(p);
    }

    void buscarSKU(int opcion)
    {
        System.out.println("Buscar por SKU");

        if (almacen.isEmpty())
        {
            System.out.println("Aun no hay productos en el inventario. Agrega uno");
        }
        else
        {
            for (Producto p : almacen)
            {
                if (opcion == p.getSKU())
                {
                    System.out.println(p.getSKU() + " - " + p.getNombre() + " - " + p.getCantidad());

                    int opt = 0;
                    int repetir = 1;

                    while (opt != 10)
                    {
                        //Mostrar el menu de opciones

                        if (repetir == 1)
                        {
                            System.out.println("Elige una de las siguientes opciones:" +
                                    "\n1. Modificar SKU" +
                                    "\n2. Modificar Nombre" +
                                    "\n3. Modificar descripcion" +
                                    "\n4. Modificar precio" +
                                    "\n5. Agregar existencia" +
                                    "\n6. Disminuir existencias" +
                                    "\n7. Eliminar" +
                                    "\n8. Regresar" +
                                    "\n9. Salir");
                        }
                        else
                        {
                            System.out.println("Vuelve a ingresar la opcion: ");
                        }
                        opt = Teclado.LeerEntero();

                        switch (opt)
                        {
                            case 1:
                                p.modificaSKU();
                                break;

                            case 2:
                                p.modificaNombre();
                                break;

                            case 3:
                                p.modificaDescripcion();
                                break;

                            case 4:
                                p.modificaPrecio();
                                break;

                            case 5:
                                p.agregarExistencia();
                                break;

                            case 6:
                                p.disminuirExistencia();
                                break;

                            case 7:
                                almacen.remove(p);
                                System.out.printf("Se elimino el producto" + p.getNombre() + "correctamen");
                                break;

                            case 8:
                                return;

                            case 9:
                                System.out.println("Hasta pronto");
                                System.exit(0);
                                break;

                            default:
                                System.out.println("Ingresaste una opcion no valida. Intenta nuevamente");
                                repetir = 0;
                                break;
                        }
                        break;
                    }
                }
            }
        }
    }

    void menuInventario()
    {
        int opcion = 0;
        int repetir = 1;

        while (opcion != 4)
        {
            if (repetir == 1)
            {
                System.out.println("Elige una de las siguientes opciones:" +
                        "\n1. Listar todos los productos" +
                        "\n2. Listar solo productos en existencia" +
                        "\n3. Listar productos agotados" +
                        "\n4. Regresar" +
                        "\n5. Salir" +
                        "\n-  Puedes Ingresa el SKU");
            }
            else
            {
                System.out.println("Vuelve a ingresar la opcion: ");
            }
            opcion = Teclado.LeerEntero();

            repetir = 1;

            if(opcion > 5)
            {
                buscarSKU(opcion);
                repetir = 1;
            }

            else
            {
                switch(opcion)
                {
                    case 1:

                    case 2:

                    case 3:
                        listarProducto(opcion);
                        break;
                    case 4:
                        System.out.println("Regresaste a menu principal");
                        break;

                    case 5:
                        System.exit(0);
                        break;

                    default:
                        System.out.println("Ingresaste una opcion no valida.");
                        repetir = 0;
                        break;
                }
            }
        }
    }


    //eliminarProducto .remove
    void eliminarProdcuto(int sku)
    {
        boolean encontrado = true;
        for(Producto p : almacen)
        {
            //Si el SKU del producto corresponde al SKU buscando...1
            if(p.getSKU() == sku)
            {
                encontrado = true;

                //Producto encontrado
                System.out.println("Producto encontrado");

                //Eliminar
                almacen.remove(p);
                System.out.println("El prodcuto a sido eliminado con éxito");
                break;
            }

            if(!encontrado)
            {
                System.out.println("No se encontró el prodcuto. Revisa su SKU");
            }
        }
    }
}