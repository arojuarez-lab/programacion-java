package com.company;

public class Main
{

    public static void main(String[] args)
    {
        //Crear almacen
        Almacen almacen = new Almacen();

        //Declaración de variables
        int opcion = 0;
        int repetir = 1;
        int sku = 0;


        while(opcion != 3)
        {
            //Menú de opciones
            if(repetir == 1)
            {
                System.out.println("Inventario, opción a ejecutar: " +
                        "\n1. Listar producto" +
                        "\n2. Agregar producto" +
                        "\n3. Salir del programa");
            }
            else
            {
                System.out.println("Vuelve a ingresar la opcion: ");
            }
            opcion = Teclado.LeerEntero();

            //(Switch)Dependiendo de la opción del usuario
            switch(opcion)
            {
                //Listar productos
                case 1:
                    almacen.menuInventario();
                    repetir = 1;
                    break;

                case 2:
                    almacen.generarProducto();
                    repetir = 1;
                    break;

                case 3:
                    System.out.println("Haz elegido salir de programa, vuelve pronto.");
                    break;

                default:
                    System.out.println("Ingresaste una opción no invalida. Intenta nuevamente.");
                    repetir = 0;
                    break;
            }
        }
    }
}
