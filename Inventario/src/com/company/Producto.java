package com.company;

public class Producto
{
    //Declaración de variables
    private int SKU;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private double precio;

    //Creamos constructor
    public Producto(int sku, String nombre, String descripcion, int cantidad, double precio)
    {
        this.SKU = sku;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    //Métodos

    //Getters
    int getSKU()
    {
        return SKU;
    }

    String getNombre()
    {
        return nombre;
    }

    String getDescripcion()
    {
        return descripcion;
    }

    int getCantidad()
    {
        return cantidad;
    }

    double getPrecio()
    {
        return precio;
    }

    // Setters
    void setSKU(int x)
    {
        SKU = x;
    }

    void setNombre(String x)
    {
        nombre = x;
    }

    void setDescripcion(String x)
    {
        descripcion = x;
    }

    void setCantidad(int x)
    {
        cantidad = x;
    }

    void setPrecio(Double x)
    {
        precio = x;
    }

    //Modificar SKU
    void modificaSKU(){
        System.out.println("Ingresa nuevo SKU");
        setSKU(Teclado.LeerEntero());
    }

    void modificaNombre()
    {
        System.out.println("Ingresa nuevo nombre");
        setNombre(Teclado.LeerCadena());
    }

    void modificaDescripcion()
    {
        System.out.println("Ingresa nueva descripcion");
        setDescripcion(Teclado.LeerCadena());
    }

    void modificaPrecio()
    {
        System.out.println("Ingresa nuevo precio");
        setPrecio(Teclado.LeerDouble());
    }

    void agregarExistencia()
    {
        System.out.println("Piezas a añadir");
        int incremento = Teclado.LeerEntero();
        while(incremento <= 0)
        {
            System.out.println("Ingresa un numero positivo");
            incremento = Teclado.LeerEntero();
        }
        int cantidad = getCantidad();
        cantidad = cantidad + incremento;
        setCantidad(cantidad);


    }

    void disminuirExistencia()
    {
        System.out.println("Ingrese el decremento");
        int decremento = Teclado.LeerEntero();
        int cantidad = getCantidad();
        cantidad = cantidad - decremento;
        setCantidad(cantidad);
    }

}