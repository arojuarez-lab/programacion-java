package com.company.Chilaquiles;

public abstract class Chilaquiles implements Pedido
{
    //Atributos encapsulados
    private String salsa;
    private String proteina;


    public Chilaquiles(String salsa, String proteína)
    {
        this.salsa = salsa;
        this.proteina = proteína;
    }

    public String getSalsa()
    {
        return salsa;
    }

    public void setSalsa(String salsa)
    {
        this.salsa = salsa;
    }

    public String getProteína()
    {
        return proteina;
    }

    public void setProteina(String proteína)
    {
        this.proteina = proteina;
    }

    public void pedidoListo()
    {
        System.out.println("*** Comanda capturada con éxito ***");
    }
    @Override
    public String toString()
    {
        String q, c, ce;
        return "\nSalsa: " + salsa + "\nProteína: " + proteina;
    }


}