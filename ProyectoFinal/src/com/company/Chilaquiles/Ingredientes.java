package com.company.Chilaquiles;

public class Ingredientes extends Chilaquiles
{
    private int queso;
    private int crema;
    private int cebolla;

    public Ingredientes(int queso, int crema, int cebolla, String salsa, String proteina)
    {
        super(salsa, proteina);
        this.queso = queso;
        this.crema = crema;
        this.cebolla = cebolla;
    }

    public void setQueso(int queso)
    {
        this.queso = queso;
    }

    public void setCrema(int crema)
    {
        this.crema = crema;
    }

    public void setCebolla(int cebolla)
    {
        this.cebolla = cebolla;
    }

    public int getQueso()
    {
        return queso;
    }

    public int getCrema()
    {
        return crema;
    }

    public int getCebolla()
    {
        return cebolla;
    }

    @Override
    public String toString()
    {
        String q, c, ce;
        if (queso == 1)
        {
            q = "Si";
        }
        else
        {
            q = "No";
        }

        if (crema == 1)
        {
            c = "Si";
        }
        else
        {
            c = "No";
        }

        if (cebolla == 1)
        {
            ce = "Si";
        }
        else
        {
            ce = "No";
        }

        return super.toString() + "\nQueso: " + q + "\nCrema: " + c + "\nCebolla: " + ce;
    }
}