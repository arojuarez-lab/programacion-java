package com.company.Chilaquiles;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Entrada
{
    //String
    public static String LeerCadena()
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        try
        {
            str = br.readLine();
        }
        catch (Exception e)
        {
            str = "";
        }
        return str;
    }

    //Entero
    public static int LeerEntero()
    {
        int num;
        try
        {
            num = Integer.parseInt(LeerCadena().trim());
        }
        catch (Exception e)
        {
            num = 0;
        }
        return num;
    }
}