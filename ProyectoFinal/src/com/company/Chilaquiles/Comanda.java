package com.company.Chilaquiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Comanda
{
    public static void main(String[] args)
    {
        int op, s, p, q, c, ce;
        int contc = 0, cm = 0;
        String salsa, proteina, queso, crema, cebolla;

        //Arreglo de los platillos
        Chilaquiles chilaquiles[] = new Chilaquiles[30];
        DateTimeFormatter fecha = DateTimeFormatter.ofPattern("yyyy/MMMM/dd HH:mm:ss");

        do
        {

            System.out.println("\n\t\t\t\t\t\t\t\t\t\t\t\t\t" + fecha.format(LocalDateTime.now()) + "\n");
            System.out.println("\t >>> ::: BIENVENIDO A CHILAQUERIA EL CHINO ::: <<< \n");
            System.out.println("INGRESE LA OPCIÓN DESEADA: " +
                    "\n1 NUEVA COMANDA" +
                    "\n2 MODIFICAR COMANDA" +
                    "\n3 MOSTRAR COMANDAS" +
                    "\n4 ELIMINAR COMANDA" +
                    "\n5 SALIR");

                op = Entrada.LeerEntero();
                switch(op)
                {
                    case 1:
                        System.out.println("Elija los elementos de la comanda: " +
                                "\n  SALSA: " +
                                "\n1 VERDE" +
                                "\n2 ROJA" +
                                "\n3 MORITA" +
                                "\n4 CHIPOTLE" +
                                "\n5 PASILLA");

                        salsa = Entrada.LeerCadena();
                        System.out.println("Elija los elementos de la comanda: " +
                                "\n  PROTEÍNA: " +
                                "\n1 COSTILLA DE REs" +
                                "\n2 MILANESA DE RES" +
                                "\n3 SALCHICHA" +
                                "\n4 HUEVO" +
                                "\n5 POLLO" +
                                "\n6 BISTEC" +
                                "\n7 LONGANIZA" +
                                "\n8 ARRACHERA" +
                                "\n9 CHISTORRA" +
                                "\n10 SUADERO");
                        proteina = Entrada.LeerCadena();
                        System.out.println("Desea incluir queso: " +
                                "\n1 Si" +
                                "\n2 No");
                        q = Entrada.LeerEntero();

                        System.out.println("Desea incluir crema: " +
                                "\n1 Si" +
                                "\n2 No");
                        c = Entrada.LeerEntero();

                        System.out.println("Desea incluir cebolla: " +
                                "\n1 Si" +
                                "\n2 No");
                        ce = Entrada.LeerEntero();

                        //Ingredientes ing = new Ingredientes(q, c, ce);
                        Chilaquiles com = new Ingredientes(q, c, ce,salsa, proteina);
                        com.pedidoListo();
                        chilaquiles[contc]= com;
                        contc++;
                        break;
                    
                    case 2:
                        if(contc > 0)
                        {
                            System.out.println("Mostrando comandas");
                            for(int i = 0; i < contc; i++)
                            {
                                System.out.println("Comanda " + (i + 1) + chilaquiles[i].toString());
                            }

                            do
                            {
                                System.out.println("\tIngresa el número de comanda a modificar");
                                cm = Entrada.LeerEntero();
                                System.out.println("\tIngresa la información nueva de la comanda");
                                System.out.println("\tElija los elementos de la comanda:" +
                                        "\n  SALSAS:" +
                                        "\n1 VERDE" +
                                        "\n2 ROJA" +
                                        "\n3 MORITA" +
                                        "\n4 CHIPOTLE" +
                                        "\n5 PASILLA");

                                salsa = Entrada.LeerCadena();
                                System.out.println("\tElija los elementos de la comanda: " +
                                        "\n  PROTEÍNA:" +
                                        "\n1 COSTILLA DE RES" +
                                        "\n2 MILANESA DE RES" +
                                        "\n3 SALCHICHA" +
                                        "\n4 HUEVO" +
                                        "\n5 POLLO" +
                                        "\n6 BISTEC" +
                                        "\n7 LONGANIZA" +
                                        "\n8 ARRACHERA" +
                                        "\n9 CHISTORRA" +
                                        "\n10 SUADERO");

                                proteina = Entrada.LeerCadena();
                                System.out.println("Desea incluir queso: " +
                                        "\n1 Si" +
                                        "\n2 No");
                                q = Entrada.LeerEntero();

                                System.out.println("Desea incluir crema: " +
                                        "\n1 Si" +
                                        "\n2 No");
                                c = Entrada.LeerEntero();

                                System.out.println("Desea incluir cebolla: " +
                                        "\n1 Si" +
                                        "\n2 No");
                                ce = Entrada.LeerEntero();

                                Chilaquiles pl = new Ingredientes(q, c, ce,salsa, proteina);
                                chilaquiles[cm - 1] = pl;
                                
                                System.out.println("Cambios realizados con éxito");
                            }
                            while(cm < 0 || cm > contc);
                        }

                        else
                        {
                            System.out.println("No hay pedidos aún :(");
                        }
                        break;
                    
                    case 3:
                        if(contc > 0)
                        {
                            System.out.println("\t >>> Mostrar lista de comandas <<<");
                            System.out.println("\t >>> Mostrando comandas <<<");
                            for(int i = 0; i < contc; i++)
                            {
                                System.out.println("\tComandas " + (i + 1) + chilaquiles[i].toString());
                            }
                        }
                        else
                        {
                            System.out.println("No hay comandas aún");
                        }
                        break;

                    case 4:
                        int b = 0;
                        if(contc > 0)
                        {
                            System.out.println("\t >>> Mostrar lista de comandas <<<");
                            System.out.println("\t >>> Mostrando comandas <<<");

                                for(int i = 0; i < contc; i++)
                                {
                                    System.out.println("\tComandas " + (i + 1) + chilaquiles[i].toString());
                                }
                                System.out.println("Ingresa número de comanda a borrar");
                                b = Entrada.LeerEntero();
                                Chilaquiles bo = new Ingredientes(0, 0, 0, " ", "");
                                chilaquiles[b - 1] = bo;
                                System.out.println("*** Comanda borrada con éxito ***");
                        }
                        else
                        {
                            System.out.println("No hay comandas para borrar aún :(");
                        }
                        break;
                    
                    case 5:
                        System.out.println("Saliendo... Gracias por su compra");
                        break;

                    default:
                        System.out.println("Ingresaste una opción no invalida. Intenta nuevamente. XD");
                        main(null);
                        break;
                }
            }
        while(op != 5);
    }
}