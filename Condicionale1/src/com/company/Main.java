package com.company;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        //Pedir un caracter
        System.out.println("Introduce una letra y te diré si es mayúscula o minúscula");
        Scanner entrada = new Scanner (System.in);
        char letra = entrada.nextLine().charAt(0);

        //Revisar si es mayúscula
        if(Character.isUpperCase(letra))
        {
            //Si sí, indicar "La letra es mayúscula"
            System.out.println("La letra es mayúscula");
        }
        else if(Character.isLowerCase(letra))
        {
            //Si no, indicar "La letra es minúscula"
            System.out.println("La letra es minúscula");
        }
        else
        {
            System.out.println("No es una letra");
        }


	 
    }
}
