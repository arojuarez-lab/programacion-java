package com.company;


public class Producto {

    //Declaracion de variables
    private int SKU;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private double precio;

    //Creamos constructor
    public Producto (int sku, String nombre, String descripcion, int cantidad, double precio){
        this.SKU = sku;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    //Metodos setters
    void setSKU(int x){
        SKU=x;
    }

    void setNombre(String x){
        nombre=x;
    }
    void setDescripcion(String x){
        descripcion=x;
    }
    void setCantidad(int x){
        cantidad=x;
    }
    void setPrecio(double x){
        precio=x;
    }



    //Metodos getters
    int getSKU(){
        return SKU;
    }

    String getNombre(){
        return nombre;
    }

    String getDescripcion(){
        return descripcion;
    }

    int getCantidad(){
        return cantidad;
    }

    double getPrecio(){
        return precio;
    }


    //Modificar atributos
    void modificaSKU(){
        System.out.println("Ingresa nuevo SKU");
        setSKU(Teclado.LeeEntero());
    }

    void modificaNombre(){
        System.out.println("Ingresa nuevo nombre");
        setNombre(Teclado.LeeCadena());
    }

    void modificaDescripcion(){
        System.out.println("Ingresa nueva descripcion");
        setDescripcion(Teclado.LeeCadena());
    }

    void modificaPrecio(){
        System.out.println("Ingresa nuevo precio");
        setPrecio(Teclado.LeeEntero());
    }

    void agregarExistencia(){
        System.out.println("¿Cuantas piezas deseeas añadir?");
        int incremento = Teclado.LeeEntero();
        while(incremento <= 0){
            System.out.println("Ingresa un numero positivo");
            incremento = Teclado.LeeEntero();
        }
        int cantidad = getCantidad();
        cantidad = cantidad + incremento;
        setCantidad(cantidad);
    }

    void disminuirExistencia(){
        System.out.println("¿Cuantas piezas deseeas eliminar?");
        int decremento = Teclado.LeeEntero();

        while(decremento >= 0){
            System.out.println("Ingresa un numero positivo");
            decremento = Teclado.LeeEntero();
        }
        int cantidad = getCantidad();
        cantidad = cantidad - decremento;
        setCantidad(cantidad);
    }




}
