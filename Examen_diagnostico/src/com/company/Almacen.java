package com.company;

import java.awt.*;
import java.util.ArrayList;

public class Almacen {

    private ArrayList<Producto> almacen = new ArrayList<Producto>();
    Menus m = new Menus();

    boolean almacenVacio(){
        return almacen.isEmpty();
    }

    void menuInventario() {
        int opcion = 0;
        int repetir = 1;

        while (opcion != 4) {
            if (repetir == 1) {
                m.imprimirMenuInventario();
            } else {
                System.out.println("Vuelve a ingresar la opcion: ");
            }
            opcion = Teclado.LeeEntero();

            repetir = 1;

            if(opcion>5){
                buscarSKU(opcion);
                repetir=1;
            }

            else{
                switch (opcion) {
                    case 1:
                    case 2:
                    case 3:
                        listarProducto(opcion);
                        break;

                    case 4:
                        System.out.println("Regresaste a menu principal");
                        break;

                    case 5:
                        System.out.println("Hasta pronto");
                        System.exit(0);
                        break;

                    default:
                        System.out.println("Ingresaste una opcion no valida.");
                        repetir = 0;
                        break;

                }
            }
        }

    }


    void buscarSKU(int opcion) {

        System.out.println("Opcion: buscar por SKU");

        if (almacen.isEmpty()) {
            System.out.println("Aun no hay productos en el inventario. Agrega uno");
        } else {
            for (Producto p : almacen) {
                if (opcion == p.getSKU()) {
                    System.out.println("El SKU: " + p.getSKU() + "\n" + "corresponde a el producto" +
                            p.getNombre() + "-" + p.getCantidad());

                    int opt = 0;
                    //evita que el menu se repita
                    int repetir = 1;

                    while (opt != 10) {
                        //Mostrar el menu de opciones

                        if (repetir == 1) {
                            m.imprimirMenuSKU();

                        } else {
                            System.out.println("Vuelve a ingresar la opcion: ");
                        }
                        opt = Teclado.LeeEntero();

                        switch (opt) {


                            case 1:
                                p.modificaSKU();
                                break;

                            case 2:
                                p.modificaNombre();
                                break;

                            case 3:
                                p.modificaDescripcion();
                                break;

                            case 4:
                                p.modificaPrecio();
                                break;

                            case 5:
                                p.agregarExistencia();
                                break;
                            case 6:
                                p.disminuirExistencia();
                                break;
                            case 7:
                                almacen.remove(p);
                                System.out.println("Se elimino el producto: " + p.getNombre() + " correctamente" );
                                break;

                            case 8:
                                //Regresa al menu anterior
                                return;
                            case 9:
                                System.out.println("Hasta pronto");
                                System.exit(0);
                                break;
                            default:
                                System.out.println("Ingresaste una opcion no valida. Intenta nuevamente");
                                repetir = 0;
                                break;


                        }

                        break;
                    }
                }

            }

        }
    }


    void agregarProducto() {

        //Ingresa Id
        int sku, cantidad;
        String nombre, descripcion;
        double precio;

        System.out.println("Ingresa su sku");
        sku = Teclado.LeeEntero();
        //Pedir Nombre
        System.out.println("Ingresa el nombre");
        nombre = Teclado.LeeCadena();
        //Pedir descripcion
        System.out.println("Ingresa la descripcion");
        descripcion = Teclado.LeeCadena();
        //Pedir cantidad
        System.out.println("Ingresa la cantidad");
        cantidad = Teclado.LeeEntero();
        //Pedir precio
        System.out.println("Ingresa el precio");
        precio = Teclado.LeeDouble();


        //Añadir su info con los setters
        Producto producto = new Producto(sku, nombre, descripcion, cantidad, precio);
        almacen.add(producto);
    }



    void listarProducto(int opcion) {

        if (opcion == 1) {
            //Si el almacen esta vacio
            if (almacenVacio()) {
                System.out.println("Aun no hay productos en el inventario. Agrega uno");
            } else { //iterar para mostrar todos los elementos del arreglo
                for (Producto p : almacen) {
                    System.out.println(p.getSKU() + "-" + p.getNombre() + "-" + p.getCantidad());
                }
            }
        }

        //Para productos en existencia
        if (opcion == 2) {
            if (almacenVacio()) {
                System.out.println("Aun no hay productos en el inventario. Agrega uno");
            } else {
                for (Producto p : almacen) {
                    if(p.getCantidad() >0) {
                        System.out.println(p.getSKU() + "-" + p.getNombre() + "-" + p.getDescripcion());
                    }
                }
            }
        }

        //Para productos agotados
        if (opcion == 3) {
            if (almacen.isEmpty()) {
                System.out.println("Aun no hay productos en el inventario. Agrega uno");
            } else {
                for (Producto p : almacen) {
                    if(p.getCantidad() ==0) {
                        System.out.println(p.getSKU() + "-" + p.getNombre() + "-" + p.getCantidad());
                    }
                }
            }
        }

    }








}
