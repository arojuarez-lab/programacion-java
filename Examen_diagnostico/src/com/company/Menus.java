package com.company;

public class Menus {

    void imprimirMenuPrincipal(){
        System.out.println("Elige una de las siguientes opciones:" +
                "\n1. Listar producto" +
                "\n2. Agregar producto" +
                "\n3. Salir del programa");
        return;
    }

    void imprimirMenuSKU() {
        System.out.println("Elige una de las siguientes opciones:" +
                "\n1. Modificar SKU" +
                "\n2. Modificar Nombre" +
                "\n3. Modificar descripcion" +
                "\n4. Modificar precio" +
                "\n5. Agregar existencia" +
                "\n6. Disminuir existencias" +
                "\n7. Eliminar" +
                "\n8. Regresar" +
                "\n9. Salir");
        return;
    }


    void imprimirMenuInventario() {
        System.out.println("Elige una de las siguientes opciones:" +
                "\n1. Listar todos los productos" +
                "\n2. Listar solo productos en existencia" +
                "\n3 Listar productos agotados" +
                "\n4. Regresar" +
                "\n5. Salir" +
                "\nO ingresa el SKU");

        return;
    }

}
