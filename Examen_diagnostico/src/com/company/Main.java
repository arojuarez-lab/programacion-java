package com.company;

public class Main {

    public static void main(String[] args) {

        //Creamos inventario
        Almacen almacen = new Almacen();
        Menus m = new Menus();

        //Switch para mostrar informacion correspondiente
        int opcion = 0;
        int repetir =1;


        while(opcion != 3 ) {
            //Mostrar el menu de opciones

            if(repetir == 1) {
                m.imprimirMenuPrincipal();
            }
            else {
                System.out.println("Vuelve a ingresar la opcion: ");
            }
            opcion = Teclado.LeeEntero();

            switch (opcion) {


                case 1:
                    //Listar producto
                    almacen.menuInventario();
                    repetir = 1;
                    break;
                case 2:
                    //Agregar producto
                    almacen.agregarProducto();
                    repetir = 1;
                    break;

                case 3:
                    System.out.println("Hasta pronto");
                    break;

                default:
                    System.out.println("Ingresaste una opcion no valida.");
                    repetir = 0;
                    break;


            }
        }
    }
}