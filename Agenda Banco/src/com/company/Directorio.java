package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Directorio {

    private ArrayList<Cliente> directorio = new ArrayList<Cliente>();

    boolean estaVacio() {
        return directorio.isEmpty();
    }

    //Generar cliente
    void generarCliente() {
        Scanner entrada = new Scanner(System.in);
        int id;
        String nombre, correo, telefono;

        //Ingresar su ID
        System.out.println("Ingresa su ID");
        id = Teclado.LeerEntero();

        //Pedir nombre
        System.out.println("Ingresa tu nombre");
        nombre = entrada.nextLine();

        //Pedir teléfono
        System.out.println("Ingresa tu teléfono");
        telefono = entrada.nextLine();

        //Pedir correo
        System.out.println("Ingresa tu correo electrónico");
        correo = entrada.nextLine();

        Cliente cliente = new Cliente(id, nombre, telefono, correo);
        agregarCliente(cliente);
    }

    //agregarCliente .add
    void agregarCliente(Cliente c) {
        directorio.add(c);
    }

    //listarClientes
    void listarCliente() {
        if (directorio.isEmpty()) {
            System.out.println("Aún no hay clientes en el directorio, Agrega uno");
        } else { //si ha clientes
            //Foreach
            for (Cliente c : directorio) {
                System.out.println(c.getID() + " - " + c.getNombre() + " - " + c.getTelefono());
            }
        }
    }

    //modificarCliente
    void modificarTelefono(int id, String tel) {
        boolean encontrado = false;

        //Buscar cliente
        for (Cliente c : directorio) {
            //Si el ID del cliente corresponde al ID buscando...1
            if (c.getID() == id) {
                //Cliente encontrado
                encontrado = true;
                System.out.println("Cliente encontrado");

                //Ya podemos modificar tus datos
                c.setTelefono(tel);
                System.out.println("Teléfono actualizado");
            }
        }
        if (!encontrado) {
            System.out.println("No se encontró el cliente solicitado. Revisa de nuevo");
        }

        //si no lo encuentra, indicar que no existe.
    }

    //eliminarCliente .remove
    void eliminarCliente(int id) {
        boolean encontrado = true;

        for (Cliente c : directorio) {
            //Si el ID del cliente corresponde al ID buscando...1
            if (c.getID() == id) {
                encontrado = true;

                //Eliminar
                directorio.remove(c);
                System.out.println("El cliente a sido eliminado con éxito");
                break;
            }
            if (!encontrado) {
                System.out.println("No se encontró el cliente. Revisa su ID");
            }
        }
    }
}