package com.company;

public class Cliente {
    private int ID;
    private String nombre;
    private String telefono;
    private String correo;

    public Cliente(int id, String nombre, String telefono, String correo) {
        this.ID = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.correo = correo;

        //El "this" indica que se refiere a la variable de la clase (atributo)
        //y no al parámetro de entrada del método.
    }

    //Setters // Set // Establecer
    void setNombre(String x) {
        nombre = x;
    }

    void setTelefono(String x) {
        telefono = x;
    }

    void setCorreo(String x) {
        correo = x;
    }

    //Getters // Get // Obtener
    int getID() {
        return ID;
    }

    String getNombre() {
        return nombre;
    }

    String getTelefono() {
        return telefono;
    }

    String getCorreo() {
        return correo;
    }

}