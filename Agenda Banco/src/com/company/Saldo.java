package com.company;

public class Saldo
{
    double saldo;

    void addSaldo(double x)
    {
        saldo = x;
    }

    void incrementarSaldo(double sueldo)
    {
        //saldo = saldo + sueldo;
        saldo += sueldo;
    }

    void decrementarSaldo(double retiro)
    {
        //saldo = saldo - retiro;
        saldo -= retiro;
    }

    //Getters // Get // Obtener
    double getSaldo()
    {
        return saldo;
    }

}
