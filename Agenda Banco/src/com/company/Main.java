package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        //Crear directorio
        Directorio directorio = new Directorio();

        //Cliente de prueba
        Cliente prueba = new Cliente( 1, "Arturo Juárez", "123456789", "123@hotmail.com");
        prueba.setTelefono("11111");
        prueba.setNombre("Arturo Juárez");
        prueba.setCorreo("123@hotmail.com");
        directorio.agregarCliente(prueba);

        //Declaración de variables
        int opcion = 0;
        int id = 0;

        while(opcion != 5)
        {
            //Menú de opciones
            System.out.println("Elige una de las siguientes opciones;" +
                    "\n1. Listar cliente" +
                    "\n2. Agregar cliente" +
                    "\n3. Modificar cliente" +
                    "\n4. Eliminar cliente" +
                    "\n5. Salir del programa");
            opcion = Teclado.LeerEntero();

            //(Switch)Dependiendo de la opción del usuario
            switch(opcion)
            {
                // - Listar Clientes directorio.listarClientes();
                case 1: directorio.listarCliente();
                    break;

                // - Agregar Cliente directorio.agregarCliente();
                case 2: directorio.generarCliente();
                    break;

                // - Modificar
                case 3:
                    //Revisar si el arreglo directorio no está vacío
                    if(directorio.estaVacio())
                    {
                        System.out.println("No hay clientes aún.");
                    }
                    else
                    {
                        //Mostrar lisatdo de clientes
                        System.out.println("Elija un cliente para modificar sus datos");
                        directorio.listarCliente();

                        //Elegir un cliente a modificar
                        System.out.println("Ingresa el ID del cliente");
                        id = Teclado.LeerEntero();

                        //Indicar el dato a modificar
                        System.out.println("Ingresa el nuevo teléfono");
                        String tel = Teclado.LeerCadena();

                        directorio.modificarTelefono(id, tel);
                    }
                    break;

                // - Eliminar
                case 4:
                    if(directorio.estaVacio())
                    {
                        System.out.println("No hay clientes en el directorio");
                    }
                    else
                    {
                        //Mostrar listado de clientes
                        System.out.println("Elige un cliente para eliminar");
                        directorio.listarCliente();

                        //Elegir un cliente a eliminar
                        System.out.println("Ingresa el ID del cliente");
                        id = Teclado.LeerEntero();

                        directorio.eliminarCliente(id);
                    }
                    break;

                //Salir
                case 5:
                    System.out.println("Salir del programa");
                    break;

                default:
                    System.out.println("Ingresaste una opción no invalida. Intenta nuevamente.");
                    main(null);
                    break;
            }
        }

        //Generar Cliente
        directorio.generarCliente();

        //Imprimir todos Clientes
        directorio.listarCliente();
    }
}