package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        //Mostrar mensjae
        System.out.println("Indica la cantidad a retirar");
        Scanner entrada = new Scanner(System.in);

        try
        {
            //Pedir la cantidad a retirar
            int cantidad = entrada.nextInt();

            //Evaluar si es múltiplo de 100
            if(cantidad % 100 == 0)
            {
                //Si sí... Entregar el dinero
                System.out.println("Aquí esta tu efectivo");
            }
            else
            {
                //Si no, indicar que ingrese solo múltiplos de 100
                System.out.println("Ingresa solo múltiplos de 100\n\n----------\n\n");
                main(null);
            }

        }
        catch(Exception e)
        {
            System.out.println("ups, Ingresa solo números");
        }


    }
}
