package com.company;

public class Promedio
{
    public static void main(String args[])
    {
        float calif1, calif2, calif3, calif4;
        float resultado;

        System.out.println("Calificacion de matemáticas");
        calif1 = Teclado.LeerFloat();

        System.out.println("Calificacion de programación");
        calif2 = Teclado.LeerFloat();

        System.out.println("Calificacion de redes");
        calif3 = Teclado.LeerFloat();

        System.out.println("Calificacion de inglés");
        calif4 = Teclado.LeerFloat();

        resultado = calif1 + calif2 + calif3 + calif4 / 4;

        System.out.println("Tuviste el promedio de: " + resultado);
    }
}
