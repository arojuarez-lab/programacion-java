package com.company;

public class HolaTeclado
{
    public static void main(String args[])
    {
        String nombre;
        int edad;
        char letra;
        float peso;

        System.out.println("Dime tu nombre");
        nombre = Teclado.LeerCadena();

        System.out.println("Dime tu edad");
        edad = Teclado.LeerEntero();

        System.out.println("La inicial de nombre");
        letra = Teclado.LeerCaracter();

        System.out.println("Ingresa tu peso");
        peso = Teclado.LeerFloat();

        System.out.println("Hola " + nombre + " tienes " + edad + " años " + "y pesas " + peso);
        System.out.println("Tu nombre empieza con la letra " + letra);

    }
}
