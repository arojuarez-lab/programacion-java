package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        //Contraseña correcta
        final String contraseñaCorrecta = "qwerty";
        byte intento = 0;

        do
        {
            //Solicitar la contraseña
            System.out.println("Hola Arturo, ingresa tu contraseña");
            Scanner entrada = new Scanner(System.in);
            String contraseñaUsuario = entrada.nextLine();

            //Ecaluar si la contraseña es "qwerty"
            if (contraseñaUsuario.toLowerCase().equals(contraseñaCorrecta.toLowerCase()))
            {
                //Si sí, acceso concedido
                System.out.println("Acceso concedido");
                break;
            }
            else
            {
                //Sí no, acceso denegado
                System.out.println("Contraseña invalida, acceso denegado.");
                intento++;
            }
        }
        while(intento < 3);
        {
            if (intento == 3)
            {
                System.out.println("Has superado el número máximo de intentos." +
                        "\n El acceso ha sido bloqueado por seguridad.");
            }
        }


    }
}
