package com.company.ClaseAbstracta;
import static java.lang.Math.PI;

public class Circulo extends Figura
{
    private double radio;

    Circulo(double radio)
    {
        this.radio = radio;
        System.out.println("Perímetro: " + obtenerPerimetro());
        System.out.println("Área: " + obtenerArea());
    }

    @Override
    double obtenerArea()
    {
        return((radio * radio) * PI);
    }

    @Override
    double obtenerPerimetro()
    {
        return (radio * 2) * PI;
    }


}