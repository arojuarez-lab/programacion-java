package com.company.ClaseAbstracta;

public abstract class Figura
{
    abstract double obtenerArea();
    abstract double obtenerPerimetro();

}