package com.company.ClaseAbstracta;

public class abstractoTest
{
    public static void main(String[] args)
    {
        Cuadrado figura1 = new Cuadrado(5);
        System.out.println("Perímetro: " + figura1.obtenerPerimetro());
        System.out.println("Área: " + figura1.obtenerArea());

        Circulo figura2 = new Circulo( 8);
        System.out.println("Perímetro: " + figura2.obtenerPerimetro());
        System.out.println("Área: " + figura2.obtenerArea());

        Triangulo figura3 = new Triangulo(8, 8,5);
        System.out.println("Perímetro: " + figura3.obtenerPerimetro());
        System.out.println("Área: " + figura3.obtenerArea());

    }
}