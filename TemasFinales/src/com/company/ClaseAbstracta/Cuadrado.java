package com.company.ClaseAbstracta;

public class Cuadrado extends Figura
{
    private double lado;

    Cuadrado(double lado)
    {
        this.lado = lado;
        System.out.println("Perímetro: " + obtenerPerimetro());
        System.out.println("Área: " + obtenerArea());
    }

    @Override
    double obtenerArea()
    {
        return lado * lado;
    }

    @Override
    double obtenerPerimetro()
    {
        return lado + lado + lado + lado;
    }

}