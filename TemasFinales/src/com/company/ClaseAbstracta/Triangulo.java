package com.company.ClaseAbstracta;

public class Triangulo extends Figura
{
    private float lado;
    private float base;
    private float altura;

    Triangulo(float lado, float base, float altura)
    {
        this.lado = lado;
        this.base = base;
        this.altura = altura;
        System.out.println("Perímetro: " + obtenerPerimetro());
        System.out.println("Área: " + obtenerArea());
    }

    @Override
    double obtenerArea()
    {
        return base * altura / 2;
    }

    @Override
    double obtenerPerimetro()
    {
        return lado + lado + lado;
    }
}