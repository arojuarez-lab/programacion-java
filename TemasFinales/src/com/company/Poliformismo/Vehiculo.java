package com.company.Poliformismo;

public abstract class Vehiculo
{
    protected String matricula;
    protected String marca;
    protected String modelo;

    Vehiculo(String matricula, String marca, String modelo)
    {
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
    }

    abstract public String mostrarDatos();

}