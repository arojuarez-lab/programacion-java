package com.company.Poliformismo;

public class PoliformismoTest
{
    public static void main(String[] args)
    {
        Vehiculo deportivo = new VehiculoDeportivo("QWER", "Ferrari", "A8", 4);
        Vehiculo turismo = new VehiculoTurismo("RTYU", "Ferrari", "A8", 4);
        Vehiculo furgoneta = new VehiculoFurgoneta("IOPA", "Ferrari", "A8", 500);

        System.out.println("Deportivo");
        System.out.println(deportivo.mostrarDatos());
        System.out.println("Turismo");
        System.out.println(turismo.mostrarDatos());
        System.out.println("Fuergoneta");
        System.out.println(furgoneta.mostrarDatos());
    }
}
