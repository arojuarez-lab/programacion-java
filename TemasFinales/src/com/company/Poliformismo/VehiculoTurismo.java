package com.company.Poliformismo;

public class VehiculoTurismo extends Vehiculo
{
    private int numeroPuertas;
    VehiculoTurismo(String matricula, String marca, String modelo, int numeroPuertas)
    {
        super(matricula, marca, modelo);
        this.numeroPuertas = numeroPuertas;
    }

    @Override
    public String mostrarDatos()
    {
        return "Marca: " + marca
            +"\nModelo: " + modelo
            +"\nMatricula: " + matricula
            +"\nNumero de puertas: " + numeroPuertas;

    }

}