package com.company.Poliformismo;

public class VehiculoFurgoneta extends Vehiculo
{
    private int carga;
    VehiculoFurgoneta(String matricula, String marca, String modelo, int carga)
    {
        super(matricula, marca, modelo);
        this.carga = carga;
    }

    @Override
    public String mostrarDatos()
    {
        return "Marca: " + marca
                +"\nModelo: " + modelo
                +"\nMatricula: " + matricula
                +"\nCarga: " + carga;

    }
}