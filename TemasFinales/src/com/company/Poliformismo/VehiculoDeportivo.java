package com.company.Poliformismo;

public class VehiculoDeportivo extends Vehiculo
{
    private int cilindrada;
    VehiculoDeportivo(String matricula, String marca, String modelo, int cilindrada)
    {
        super(matricula, marca, modelo);
        this.cilindrada = cilindrada;
    }

    @Override
    public String mostrarDatos()
    {
        return "Marca: " + marca
                +"\nModelo: " + modelo
                +"\nMatricula: " + matricula
                +"\nCilindrada: " + cilindrada;

    }

}