package com.company.Repaso;

public class Producto
{
    private String sku;
    private String nombre;
    private int cantidad;
    static int contador; //este atributo le pertenece a la clase

    public Producto(String sku, String nombre, int cantidad)
    {
        this.sku = sku;
        this.nombre = nombre;
        this.cantidad = cantidad;

        contador += 1;
    }

}