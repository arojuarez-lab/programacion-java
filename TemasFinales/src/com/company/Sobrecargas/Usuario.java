package com.company.Sobrecargas;

//Ejemplo de sobrecarga de métodos

public class Usuario
{
    static int contador = 0;

    String nombre;
    int edad;
    int id;
    double saldo;

    Usuario()
    {
        contador++;
        id = contador;
    }

    Usuario(String nombre, int edad)
    {
        this();
        this.nombre = nombre;
        this.edad = edad;
    }

    Usuario(String nombre)
    {
        this();
        this.nombre = nombre;
        System.out.println("Recuerda preguntarle la edad para tener sus datos actualizados");
    }

    void imprimirDatos()
    {
        System.out.println("Nombre: " + nombre);
        System.out.println("Edad: " + edad);
    }

    void imprimirDatos(String dato)
    {
        switch(dato)
        {
            case "todo":
                imprimirDatos();
                break;

            case "nombre":
                System.out.println("Nombre: " + nombre);
                break;

            case "edad":
                System.out.println("Edad: " + edad);
                break;

            default:
                break;
        }
    }

    void imprimirDatos(int dato)
    {
        switch(dato)
        {
            case 1:
                System.out.println("Nombre: " + nombre);
                break;

            case 2:
                System.out.println("Edad: " + edad);
                break;

            default:
                break;
        }
    }
}
