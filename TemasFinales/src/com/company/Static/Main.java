package com.company.Static;

import static java.lang.Math.*;

public class Main
{
    public static void main(String[] args)
    {
        //Variables estáticas

        /*
        StaticDemo.y = 5;

        StaticDemo staticdemo1 = new StaticDemo();
        staticdemo1.x = 10;

        StaticDemo staticdemo2 = new StaticDemo();
        staticdemo2.y = 5;

        System.out.println(staticdemo1.suma());
        System.out.println(staticdemo2.suma());
        */

        //Métodos estáticos
        //System.out.println(StaticDemo.dividir());

        //Bloques estáticos
        StaticDemo staticdemo1 = new StaticDemo("Este es un mensaje");
        System.out.println("La raiz cuadrada de 2 es: " + StaticDemo.raiz2);
        System.out.println("La raiz cuadrada de 3 es: " + StaticDemo.raiz3);

    }
}
