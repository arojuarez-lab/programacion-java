package com.company.Static;

import static java.lang.Math.*;

public class StaticDemo
{
    //Variables estaticas

    /*int x;
    static int y;

    int suma()
    {
        return x + y;
    }*/

    //Métodos estáticos

    /*
    static int valor = 1024;

    static int dividir()
    {
        return valor / 2;
    }

     */

    //Bloques estáticos
    static double raiz2;
    static double raiz3;

    static
    {
        System.out.println("Este es un bloque estático");
        raiz2 = Math.sqrt(2.0);
        raiz3 = Math.sqrt(3.0);
    }

    StaticDemo(String mensaje)
    {
        System.out.println(mensaje);
    }
}
