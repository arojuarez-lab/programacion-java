package com.company.Herencia;

public class Estudiante extends Persona
{
    private int codigoEstudiante;
    private double notaFinal;

    public Estudiante(String nombre, String apellido, int edad, int codigoEstudiante, double notaFinal)
    {
        super(nombre, apellido, edad);
        this.codigoEstudiante = codigoEstudiante;
        this.notaFinal = notaFinal;
    }

    
    public int getCodigoEstudiante()
    {
        return codigoEstudiante;
    }

    public double getNotaFinal()
    {
        return notaFinal;
    }


    public void setCodigoEstudiante(int codigoEstudiante)
    {
        this.codigoEstudiante = codigoEstudiante;
    }

    public void setNotaFinal(float notaFinal)
    {
        this.notaFinal = notaFinal;
    }

    //Sobreescritura de mostrar datos
    @Override
    void mostrarDatos()
    {
        super.mostrarDatos();
        System.out.println("Código de Estudiante: " + getCodigoEstudiante() + " Nota Final: " + getNotaFinal());
    }
}
