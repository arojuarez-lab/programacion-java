package com.company;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
	    //Mostrar menú de opciones
        System.out.println("Elige una de las siguientes hambiurguesas del menú;" +
                "\n1. Hamburguesa con queso" +
                "\n2. Hamburguesa con queso y tocino" +
                "\n3. Hamburguesa con doble carne" +
                "\n4. Hamburguesa vegetariana" +
                "\n5. Hamburguesa con refresco y papas");

        //Solicitar que entre alguna
        Scanner entradas = new Scanner(System.in);
        int opcion = 0;

        try
        {
            opcion = entradas.nextInt();
        }
        catch (Exception e)
        {
            //System.out.println("Ingresaste una opción nmo invalida. Intenta nuevamente.");
        }
        //Switch para mostrar información correspondiente
        switch(opcion)
        {
            case 1:
                System.out.println("$40");
                break;
            case 2:
                System.out.println("$50");
                break;
            case 3:
                System.out.println("55");
                break;
            case 4:
                System.out.println("60");
                break;
            case 5:
                System.out.println("80");
                break;
            default:
                System.out.println("Ingresaste una opción no invalida. Intenta nuevamente.");
                main(null);
                break;
        }

    }

}
