package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        //Contraseña correcta
        final String contraseñaCorrecta = "qwerty";
        int i =0;


        for(i = 0; i < 3; i++)
        {
            //Solicitar la contraseña
            System.out.println("Hola Arturo, ingresa tu contraseña");
            Scanner entrada = new Scanner(System.in);
            String contraseñaUsuario = entrada.nextLine();

            //Ecaluar si la contraseña es "qwerty"
            if (contraseñaUsuario.toLowerCase().equals(contraseñaCorrecta.toLowerCase()))
            {
                //Si sí, acceso concedido
                System.out.println("Acceso concedido");
                break;
            } else
            {
                //Sí no, acceso denegado
                System.out.println("Contraseña invalida, acceso denegado.");
            }
            if (i == 3)
            {
                System.out.println("Has superado el número máximo de intentos." +
                        "\n El acceso ha sido bloqueado por seguridad.");
            }
        }
    }
}
